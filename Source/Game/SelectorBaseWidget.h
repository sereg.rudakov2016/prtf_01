// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SelectorBaseWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API USelectorBaseWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	USelectorBaseWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeTick(const FGeometry &MyGeometry, float InDeltaTime) override;

	UFUNCTION()
	void TimelineStep(int PointIndex);

	UFUNCTION()
		void MovePointIndex(bool bReverse);

	UFUNCTION()
		void BlendCanvasSlot(class USelectorItemBaseWidget* Point, class UOverlay* OverlayFrom, class UOverlay* OverlayTo, float Alpha);

	UFUNCTION()
		float BlendFloat(float A, float B, float Alpha);

	UFUNCTION()
		float GetNextContent(float Value);

	UFUNCTION()
		float GetPrevContent(float Value);

	UFUNCTION()
		void TimelineStart();

	UFUNCTION()
		void TimelineStop();

	UFUNCTION(BlueprintCallable, Category = "MyWidget")
		void Switch(int GetStep);

	UFUNCTION(BlueprintCallable, Category = "MyWidget")
		void InitSelector(TArray<class UOverlay*> GetSlots, class UCanvasPanel* Canvas, int GetItemsAmount, TSubclassOf<class USelectorItemBaseWidget> ItemWidgetClass);


	UPROPERTY()
		int ItemsAmount;

	UPROPERTY()
		int Step;

	UPROPERTY()
		bool bTimelineEnable;

	UPROPERTY()
		bool bCanSwitch;

	UPROPERTY()
		float TimelineTime = 0.0;

	UPROPERTY()
		float TimelineDuration = 0.5;

	UPROPERTY()
		TArray<class USelectorItemBaseWidget*> Points;


	UPROPERTY()
		TArray<class UOverlay*> Slots;

	UPROPERTY()
		TArray<int> PointOnSlots;



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MyWidget")
		class UCurveFloat* TimelineCurve;


};
