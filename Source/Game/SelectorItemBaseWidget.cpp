// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectorItemBaseWidget.h"

USelectorItemBaseWidget::USelectorItemBaseWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer) { }

void USelectorItemBaseWidget::UpdateIndex(int index)
{
	indexItem = index;
	UpdateContent(index);
}
