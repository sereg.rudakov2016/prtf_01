// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SelectorItemBaseWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API USelectorItemBaseWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	USelectorItemBaseWidget(const FObjectInitializer& ObjectInitializer);



	UFUNCTION(BlueprintImplementableEvent)
		void UpdateBlend(float slotBlend, float PositionNormalized);

	UFUNCTION(BlueprintImplementableEvent)
		void UpdateContent(int indexUpd);

	UFUNCTION()
		void UpdateIndex(int index);

	UPROPERTY()
		int indexItem;
};
