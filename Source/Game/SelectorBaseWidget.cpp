// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectorBaseWidget.h"
#include "Components/Overlay.h"
#include "Curves/CurveFloat.h"
#include "SelectorItemBaseWidget.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/CanvasPanelSlot.h"
#include "Math/UnrealMathUtility.h"
#include "Components/CanvasPanel.h"
#include "SelectorItemBaseWidget.h"
#include "Kismet/KismetMathLibrary.h"

USelectorBaseWidget::USelectorBaseWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer) { }

void USelectorBaseWidget::NativeTick(const FGeometry &MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bTimelineEnable)
	{

		TimelineTime = TimelineTime + InDeltaTime;

		if (TimelineTime > TimelineDuration)
		{
			TimelineStop();
		}
		else
		{
			for (int i = 0; i < Points.Num(); i++)
			{

				TimelineStep(i);
			}

		}
	}


}

void USelectorBaseWidget::TimelineStep(int PointIndex)
{
	int LocalPointIndex = PointOnSlots[PointIndex];
	float LocalAlpha = TimelineCurve->GetFloatValue(TimelineTime / TimelineDuration);

	UOverlay* LocalFrom = Slots[LocalPointIndex - Step];	
	UOverlay* LocalTo = Slots[LocalPointIndex];

	BlendCanvasSlot(Points[PointIndex], LocalFrom, LocalTo, LocalAlpha);

	float A = PointOnSlots[PointIndex] - Step;
	float B = PointOnSlots[PointIndex];

	float PosNormalized = (UKismetMathLibrary::Lerp(A, B, LocalAlpha) / PointOnSlots.Num());
	Points[PointIndex]->UpdateBlend(LocalAlpha, PosNormalized);
}

void USelectorBaseWidget::MovePointIndex(bool bReverse)
{
	int LocalIndex;
	int LocalFinalIndex;

	for (int i = 0; i < PointOnSlots.Num(); i++)
	{
		if (bReverse)
		{
			if (Slots.IsValidIndex(PointOnSlots[i] - 1))
			{
				LocalIndex = PointOnSlots[i] - 1;
			}
			else
			{
				LocalIndex = Slots.Num() - 1;
			}

		    if (LocalIndex == Slots.Num() - 1)
		    {
			    LocalFinalIndex = Slots.Num() - 2;

				
			    Points[i]->UpdateIndex(GetNextContent(i));
				
		    }
		    else 
		    {
			    LocalFinalIndex = LocalIndex;
				
		    }
		}
		else 
		{
			if (Slots.IsValidIndex(PointOnSlots[i] + 1))
			{
				LocalIndex = PointOnSlots[i] + 1;
			}
			else
			{
				LocalIndex = PointOnSlots[i] + 1 - Slots.Num();
			}

			if (LocalIndex == 0)
			{
				LocalFinalIndex = 1;

				Points[i]->UpdateIndex(GetPrevContent(i));
				
			}
			else
			{
				LocalFinalIndex = LocalIndex;
				
			}
		}



		
		PointOnSlots[i] = LocalFinalIndex;
	}

}

void USelectorBaseWidget::Switch(int GetStep)
{
	if (bCanSwitch) 
	{
		Step = GetStep;
		MovePointIndex(Step == -1);
		TimelineStart();
	}

}



void USelectorBaseWidget::BlendCanvasSlot(USelectorItemBaseWidget * Point, UOverlay * OverlayFrom, UOverlay * OverlayTo, float Alpha)
{
	FMargin InOffset;

	UCanvasPanelSlot * CanvasPanel = UWidgetLayoutLibrary::SlotAsCanvasSlot(Point);
	UCanvasPanelSlot * CanvasPanelLocalFrom = UWidgetLayoutLibrary::SlotAsCanvasSlot(OverlayFrom);
	UCanvasPanelSlot * CanvasPanelLocalTo = UWidgetLayoutLibrary::SlotAsCanvasSlot(OverlayTo);

	FMargin InOffsetLocalFrom = CanvasPanelLocalFrom->GetOffsets();
	FMargin InOffsetLocalTo = CanvasPanelLocalTo->GetOffsets();


	InOffset.Left = BlendFloat(InOffsetLocalFrom.Left, InOffsetLocalTo.Left, Alpha);
	InOffset.Top = BlendFloat(InOffsetLocalFrom.Top, InOffsetLocalTo.Top, Alpha);
	InOffset.Right = BlendFloat(InOffsetLocalFrom.Right, InOffsetLocalTo.Right, Alpha);
	InOffset.Bottom = BlendFloat(InOffsetLocalFrom.Bottom, InOffsetLocalTo.Bottom, Alpha);

	CanvasPanel->SetOffsets(InOffset);

}

float USelectorBaseWidget::BlendFloat(float A, float B, float Alpha)
{


	float Blend;

	Blend = FMath::InterpEaseInOut(A, B, Alpha, 1.1);

	return Blend;
}

float USelectorBaseWidget::GetNextContent(float Value)
{
	
	int LocalID = 0;
	if (PointOnSlots.IsValidIndex(Value + 1))
	{
		LocalID = Value + 1;
	}
	if (Points[LocalID]->indexItem - 1 < 0) 
	{
		return ItemsAmount-1;
	}
	else 
	{
		
		return Points[LocalID]->indexItem - 1;
	}

}

float USelectorBaseWidget::GetPrevContent(float Value)
{

	int LocalID = PointOnSlots.Num()-1;
	if (PointOnSlots.IsValidIndex(Value - 1))
	{
		LocalID = Value - 1;
	}
	if (Points[LocalID]->indexItem + 1 >= ItemsAmount)
	{
		return 0.0f;
	}
	else
	{
		return Points[LocalID]->indexItem + 1;
	}
}

void USelectorBaseWidget::TimelineStart()
{
	bTimelineEnable = true;
	TimelineTime = 0.0;
}

void USelectorBaseWidget::TimelineStop()
{
	bTimelineEnable = false;
}


void USelectorBaseWidget::InitSelector(TArray<class UOverlay*> GetSlots, UCanvasPanel* Canvas, int GetItemsAmount, TSubclassOf<class USelectorItemBaseWidget> ItemWidgetClass)
{
	Slots = GetSlots;
	ItemsAmount = GetItemsAmount;
	for (int i = 0; i < GetSlots.Num() - 1; i++) 
	{
		if (ItemWidgetClass)
		{
			USelectorItemBaseWidget* ItemWidget = CreateWidget<USelectorItemBaseWidget>(GetWorld(), ItemWidgetClass);
			if (ItemWidget)
			{
				Points.Add(ItemWidget);
				
				Canvas->AddChildToCanvas(ItemWidget)->SetAnchors(FAnchors(0.0, 0.0, 1.0, 1.0));
			}
		}
	}

	for (int i = Points.Num() - 1; i > -1; i--)
	{
		Points[i]->UpdateContent(i);
		PointOnSlots.Add(i);
	}


	TimelineStart();

	bCanSwitch = ItemsAmount > Slots.Num() - 2;
	if (!(ItemsAmount > Slots.Num() - 2)) 
	{
		for (int i = 0; i < PointOnSlots.Num(); i++)
		{
			if (i >= ItemsAmount) 
			{
				Points[i]->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}


}


